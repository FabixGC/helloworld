package fabix.helloworld.modelo.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Fabian on 28/08/2017.
 */

public class HolaMundoDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME ="HolaMundoAPR4DB.db";
    public static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE =
            "CREATE TABLE" + HolaMundoDBContract.HolaMundoDBUsuarios.TABLE_NAME +
                "(" +HolaMundoDBContract.HolaMundoDBUsuarios._ID +" INTEGER PRIMARY KEY," +
                HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME + " TEXT," +
            HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD +"TEXT)";

    public HolaMundoDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
